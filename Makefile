	
uf1: uf1.o disjointSet.o 
	g++ uf1.o disjointSet.o -o uf1
uf1.o: uf1.cpp
	g++ -c uf1.cpp
uf2: uf2.o disjointSet.o 
	g++ uf2.o disjointSet.o -o uf2
uf2.o: uf2.cpp
	g++ -c uf2.cpp
disjointSet.o: disjointSet.cpp disjointSet.h
	g++ -c disjointSet.cpp disjointSet.h
gen: gen.o
	g++ gen.o -o gen
gen.o: gen.cpp 
	g++ -c gen.cpp 
clean:
	rm *o uf1 uf2 gen *gch
